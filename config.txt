Option :Training
Nb_data_entrainement :3
Nom_data-train  :tr_ZFSSensor1_split_1-class-Feature25.las
                         :tr_ZFSSensor1_split_5-class-Feature25.las
                         :tr_ZFSSensor1_split_6-class-Feature25.las
Nom_data-test   :tr_ZFSSensor1_split_4-class-Feature25.las
Nom_Feature :Verticality16
            :Verticality25
            :Linearity16
            :Planarity25
            :Planarity16
            :Numberneighbors25
            :Anisotropy25
            :Anisotropy10
            :Surfacevariation25
            :Sphericity25
            :Sphericity10
Nom_class   :classification
Calcul_plusieur_modèle :False
Nb_estimators  :[10]
Nb_min_samples :[2]
Nb_min_leaf    :[1]
Save_model     :True
Nom_model      :model_Test-2
Régularisation :False
Nombre_point_voisinage :500
Pourcentage :0.85