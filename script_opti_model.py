# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 15:36:46 2022

@author: Rougeau Camille
"""

import time
import pickle
import itertools
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import jaccard_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.ensemble import AdaBoostClassifier
from sklearn.datasets import make_classification
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
import laspy
from sklearn.neighbors import KDTree
from sklearn.neighbors import NearestNeighbors
import matplotlib.pyplot as plt




def ouverture_fichier_config(dataname):
    """Ouverture du fichier de config et récupération des différents paramètres

    Args:
        dataname (_String_): Nom (chemin) du fichier de config

    Returns:
        _list_: _Retourne les paramètres sous forme de liste_
    """
    with open(dataname,'rt')as f:
        L=f.readlines()
        Lconf=[]           
        for l in L:
            tab=l.split('\n')
            test=tab[0].split(':')
            Lconf.append(test[1])
    return Lconf
    

    
def ouverture_fichier_coordonner(dataname):
    """ Ouverture des fichiers las pour récupérer les coordonnées x,y,z du nuage de point 

    Args:
        dataname (_string_): Nom (chemin du fichier) las

    Returns:
        _array_: array comportant les coordonnées des points 
    """
    with laspy.open(dataname) as fh:
        las = fh.read()
        dataset=np.vstack((las.X,las.Y,las.Z))
    return dataset.T
    

def train_model(X_train, Y_train, n_estimators, max_depth, n_jobs,critere,split,leaf):
    ''' Train the Random Forest model with the specified parameters and return it
        Attributes:
            X_train (np.array)  :   Training data
            Y_train (np.array)  :   Training classes
            n_estimators (int)  :   Number of trees in the forest
            max_depth (int)     :   Maximum depth of each tree
            n_jobs (int)        :   Number of threads used to train the model
        
        Return:
            model (np.RandomForestClassifier)  :   trained model
    '''
    model = RandomForestClassifier(n_estimators=n_estimators, max_depth=max_depth, random_state=0, oob_score=True, n_jobs=n_jobs, criterion=critere,min_samples_split=split,min_samples_leaf=leaf)
    print('entrainement...')
    model.fit(X_train, Y_train)         # Use only the specified features. 
    return model 



def calcul_voisinage(x0,y0,z0,data,tree,point,coordonner,nb_point):
    """_summary_ Fonction qui calcule les points au voisinage de celui qui a pour coordonnées (x0,y0,z0)

    Args:
        x0 (_float_): coordonné x du point au centre
        y0 (_float_): coordonné y du point au centre
        z0 (_float_): coordonné z du point au centre
        data (_array_): dataset
        tree (_type_): KDTree calculé par scikit-learn
        point (_type_): _description_
        coordonner (_array_): coordonnées x,y,z des données
        nb_point (_int_): nb de point calculé au voisinage 

    Returns:
        _list_: _list qui contient les points au voisinage _
    """
    point= point.reshape(1,-1)
    dist,ind =tree.query(point,k=nb_point)
    tab=[]
    for i in range(nb_point):
        tab.append(data[ind[0,i],-1])
    return tab
    
    
def compute_classification(tableau_sphere):
    """Fonction qui calcule le pourcentage de chaque classe des points environnant

    Args:
        tableau_sphere (_list_): _list des points au voisinage 

    Returns:
        _list_: List qui contient le pourcentage de chaque classe dans les points environnant 
    """
    compute=[]
    if len(tableau_sphere)>0:
        tableau_sphere=list(map(int, tableau_sphere))
        for k in range(max(tableau_sphere)+1):
            compute.append(tableau_sphere.count(k))
        return compute
    return compute


def point_perdu_boolean(compute,valeur_ratio):
    """
    Fonction qui détermine si le point est "perdue " ou non

    Args:
        compute (_list_): List qui contient le pourcentage de chaque classe dans les points environnant 
        valeur_ratio (_float_): Pourcentage a partir duquel le point est considéré comme perdue 

    Returns:
        _boolean_: _True si le point est perdue False sinon_
    """
    max_compute=0
    indice_max=0
    point_perdu=False
    for k in range(len(compute)):
        if compute[k]>max_compute:
            max_compute=compute[k]
            indice_max=k
    ratio=max_compute/sum(compute)
    if ratio>valeur_ratio:
        point_perdu=True
    return point_perdu,indice_max

def regularisation_classif_methode_point_perdu(data,r,valeur_ratio,coordonner,nb_point,tree):
    nombre_point,nombre_feature=np.shape(data)
    for j in range(nombre_point):
        tableau_sphere=calcul_voisinage(data[j,0],data[j,1],data[j,2],data,tree,coordonner[j],coordonner,nb_point)
        compute=compute_classification(tableau_sphere)
        if len(compute)>0:
            point_perdu,valeur=point_perdu_boolean(compute,valeur_ratio)
            if point_perdu==True:
                data[j,-1]=valeur
    return data


def Recuperation_feature(Lconfi,dataname):
    """ Récupération des features présent dans le fichier de config

    Args:
        Lconfi (_list_): List des paramètres récupéré dans le fichier de config
        dataname (_String_): Nom du fichier las

    Returns:
        _array_: Array comportant les données utilisé pour l'entrainement ou classification
    """
    with laspy.open(dataname) as fh:
        las = fh.read()
        dataset=np.vstack((las.X))
        dataset=dataset.T
        print(dataset)
        if 'green' in Lconfi:
             dataset=np.vstack((dataset,las.green))
        if 'red' in Lconfi:
            dataset=np.vstack((dataset,las.red))
        if 'blue' in Lconfi:
            dataset=np.vstack((dataset,las.blue))
        if 'Verticality16' in Lconfi:
            dataset=np.vstack((dataset,las.Verticality16))
        if 'Verticality25' in Lconfi:
            dataset=np.vstack((dataset,las.Verticality25))
        if 'Verticality8' in Lconfi:
            dataset=np.vstack((dataset,las.Verticality8))
        if 'Linearity16' in Lconfi:
            dataset=np.vstack((dataset,las.Linearity16))
        if 'Linearity25' in Lconfi:
            dataset=np.vstack((dataset,las.Linearity25))
        if 'Linearity8' in Lconfi:
            dataset=np.vstack((dataset,las.Linearity8))
        if 'Planarity25' in Lconfi:
            dataset=np.vstack((dataset,las.Planarity25))
        if 'Planarity16' in Lconfi:
            dataset=np.vstack((dataset,las.Planarity16))
        if 'Planarity8' in Lconfi:
            dataset=np.vstack((dataset,las.Planarity8))
        if 'Numberneighbors25' in Lconfi:
            dataset=np.vstack((dataset,las.Numberneighbors25))
        if 'Numberneighbors5' in Lconfi:
            dataset=np.vstack((dataset,las.Numberneighbors25))
        if 'Numberneighbors8' in Lconfi:
            dataset=np.vstack((dataset,las.Numberneighbors8))
        if 'Numberneighbors16' in Lconfi:
            dataset=np.vstack((dataset,las.Numberneighbors16))
        if 'Anisotropy25' in Lconfi:
            dataset=np.vstack((dataset,las.Anisotropy25))
        if 'Anisotropy10' in Lconfi:
            dataset=np.vstack((dataset,las.Anisotropy10))
        if 'Anisotropy5' in Lconfi:
            dataset=np.vstack((dataset,las.Anisotropy5))
        if 'Anisotropy16' in Lconfi:
            dataset=np.vstack((dataset,las.Anisotropy16))
        if 'Surfacevariation25' in Lconfi:
            dataset=np.vstack((dataset,las.Surfacevariation25))
        if 'Surfacevariation16' in Lconfi:
            dataset=np.vstack((dataset,las.Surfacevariation16))
        if 'Surfacevariation8' in Lconfi:
            dataset=np.vstack((dataset,las.Surfacevariation8))
        if 'Surfacevariation10' in Lconfi:
            dataset=np.vstack((dataset,las.Surfacevariation10))
        if 'Surfacevariation5' in Lconfi:
            dataset=np.vstack((dataset,las.Surfacevariation5))
        if 'Sphericity25' in Lconfi:
            dataset=np.vstack((dataset,las.Sphericity25))
        if 'Sphericity10' in Lconfi:
            dataset=np.vstack((dataset,las.Sphericity10))
        if 'Sphericity5' in Lconfi:
            dataset=np.vstack((dataset,las.Sphericity5))
        if 'Sphericity16' in Lconfi:
            dataset=np.vstack((dataset,las.Sphericity16))
        if 'Sphericity8' in Lconfi:
            dataset=np.vstack((dataset,las.Sphericity8))

        dataset=dataset.T
        return dataset[:,1:]


    

def Recup_classe(Lconfi,dataname):
    """ Récupération des classes données au points dans les données d'entrainement

    Args:
       Lconfi (_list_): List des paramètres récupéré dans le fichier de config
        dataname (_String_): Nom du fichier las

    Returns:
        _array_: Array comportant les classes des données
    """
    with laspy.open(dataname) as fh:
        las = fh.read()
        if Lconfi[-10]=='classification':
            classif=np.array([las.classification])
        if Lconfi[-10]=='labels':
            classif=np.array([las.labels])
        return classif


            
        
                

def programme_classification(dataname):
    """ Programme qui fait tourner l'entrainement ou la classification selon le fichier de config et apelle les autres fonctions

    Args:
        dataname (_String_): nom (chemin) du fichier de config
    """
    Lconfi=ouverture_fichier_config(dataname)
    if Lconfi[0]=='Training':
        nombre_data_Training=int(Lconfi[1])
        dataset=Recuperation_feature(Lconfi,Lconfi[2])
        classif=Recup_classe(Lconfi,Lconfi[2])
        print('Loading data')
        for k in range(1,nombre_data_Training):
            dataname=Lconfi[k+2]
            print(dataname)
            dataset_part=Recuperation_feature(Lconfi,dataname)
            classif_part=Recup_classe(Lconfi,dataname)
            dataset=np.vstack((dataset,dataset_part))
            classif=np.vstack((classif.T,classif_part.T))
            classif=classif.T
        classif=classif.T
        a,b=np.shape(dataset)
        print('Le nombre de point calculé est', a)
        nametest=Lconfi[2+nombre_data_Training]
        datatest=Recuperation_feature(Lconfi,nametest)
        classiftest=Recup_classe(Lconfi,nametest)
        print("Correction des nan présent dans les données ")
        dataset=np.nan_to_num(dataset)
        datatest=np.nan_to_num(datatest)
        list_criterion=["entropy"]
        list_nb_estimators=eval(Lconfi[-8])
        list_nb_split=eval(Lconfi[-7])
        list_nb_leaf=eval(Lconfi[-6])
        list_f1=[]
        best_f1=0
        best_ne=0
        best_crit='aucun'
        best_split=0
        best_leaf=0
        if Lconfi[-9]=='False':
            model,f1=test_model(dataset,classif,datatest,classiftest,list_nb_estimators[0],None,list_criterion[0],list_nb_split[0],list_nb_leaf[0])
        else :
            for i in range(len(list_criterion)):
                for j in range(len(list_nb_split)):
                    for l in range(len(list_nb_leaf)):
                        for k in range(len(list_nb_estimators)):
                            model,f1=test_model(dataset,classif,datatest,classiftest,list_nb_estimators[k],None,list_criterion[i],list_nb_split[j],list_nb_leaf[l])
                            print(model.feature_importances_)
                            print(f1,list_nb_estimators[k])
                            list_f1.append(f1)
                            if f1> best_f1:
                                best_f1=f1
                                best_ne=list_nb_estimators[k]
                                best_crit=list_criterion[i]
                                best_split=list_nb_split[j]
                                best_leaf=list_nb_leaf[l]
                            print(best_ne,best_crit,best_split,best_leaf)
            print("Calcul du model le plus opti")
            model,f1=test_model(dataset,classif,datatest,classiftest,best_ne,None,best_crit,best_split,best_leaf)
        if Lconfi[-5]=='True':
            print("Sauvegarde du meilleur model")
            save_model(model,Lconfi[-4])
        if Lconfi[-5]=='False':
            pass
        else :
            print("Warning, vous n'avez mis ni True ni False pour Save_model, cela peut provoquer des erreurs")    

       
        if Lconfi[-3]=='True':
            Classif_predict= model.predict(datatest)
            coordonner=ouverture_fichier_coordonner(nametest)
            datas_classif=np.vstack((coordonner.T,datatest.T,Classif_predict.T))
            datas_classif=np.nan_to_num(datas_classif.T)
            print("Mise en Place d'une régularisation")
            coordonner_data=datas_classif[:,0:3]
            tree=KDTree(coordonner_data,leaf_size=2)
            dataset_regularizer=regularisation_classif_methode_point_perdu(datas_classif, 800000, float(Lconfi[-1]),coordonner_data,int(Lconfi[-2]),tree)
            nouv_predict=dataset_regularizer[:,-1]
            print('En employant une régularisation avec un calcul de nombre de point de 500 et 0.85 on peut obtenir comme résultat')
            print(confusion_matrix(np.ravel(classiftest), np.ravel(nouv_predict)))
            f1 = f1_score(np.ravel(classiftest), np.ravel(nouv_predict), average='weighted')
            print(f1)
        else : 
            print("Il est possible que vous n'avez pas demandé de régularisation au alors il y'a une erreur dans votre fichier de config")
    if Lconfi[0]=='Classer':
        print("Classification des dalles confié ")
        print("Loading model")
        model= read_model(Lconfi[1])
        nombre_data_class=int(Lconfi[2])
        print("Loading data")
        for k in range(nombre_data_class):
            namedata=Lconfi[k+3]
            print(namedata)
            data=Recuperation_feature(Lconfi,namedata)
            coordonner=ouverture_fichier_coordonner(namedata)
            data=np.nan_to_num(data)
            print("Prédiction")
            classif=model.predict(data)
            datas_classif=np.vstack((coordonner.T,data.T,classif.T))
            datas_classif=np.nan_to_num(datas_classif.T)
            if Lconfi[-3]=='True':
                print("Mise en Place d'une régularisation")
                coordonner_data=datas_classif[:,0:3]
                tree=KDTree(coordonner_data,leaf_size=2)
                dataset_regularizer=regularisation_classif_methode_point_perdu(datas_classif, 800000, float(Lconfi[-1]),coordonner_data,int(Lconfi[-2]),tree)
                header = laspy.LasHeader(point_format=3, version="1.2")
                header.add_extra_dim(laspy.ExtraBytesParams(name='Classification', type=np.int32))
                header.offsets= np.min(coordonner_data, axis=0)
                header.scales= np.array([0.1 , 0.1 , 0.1])
                new_las = laspy.LasData(header)
                new_las.x=coordonner_data[:,0]
                new_las.y=coordonner_data[:,1]
                new_las.z=coordonner_data[:,2]
                new_las.Classification=dataset_regularizer[:,-1]
                new_las.write('dalle_classer'+str(k)+'.las')
            if Lconfi[-3]=='False':
                header = laspy.LasHeader(point_format=3, version="1.2")
                header.add_extra_dim(laspy.ExtraBytesParams(name='Classification', type=np.int32))
                header.offsets= np.min(coordonner, axis=0)
                header.scales= np.array([0.1 , 0.1 , 0.1])
                print(datas_classif)
                new_las = laspy.LasData(header)
                new_las.x=datas_classif[:,0]
                new_las.y=datas_classif[:,1]
                new_las.z=datas_classif[:,2]
                new_las.Classification=datas_classif[:,-1]
                new_las.write('dalle_classer'+str(k)+'.las')
                np.savetxt('dalle_classer.csv', datas_classif, delimiter=",")
            else :
                print("Si Une erreur est apparue il est possible que vous n'avez donné ni True ni False pour la valeur régularisation , ou alors vous avez rajouté des lignes vides sur le fichier de config, vérifiez bien son contenue et qu'il soit au bon format")
    else :
        print('Si Une erreur est apparue vous avez surement rentrer aucune des options Training ou Classer dans la première ligne du fichier de config')

       
                    
    
    
        
        
def read_model(filepath):
    """ Lecture du modèle

    Args:
        filepath (_String_): nom (chemin) du fichier modèle

    Returns:
        _pickle_: modèle de classification
    """
    return pickle.load(open(filepath, 'rb'))

    
    

def save_model(model,filename) :
    """Sauvegarde du modèle créer

    Args:
        model (_model_): modèle créer grace aux données d'entrainement
        filename (_type_): nom du fichier enregistré
    """
    with open(filename, 'wb') as out:
        pickle.dump(model, out, pickle.HIGHEST_PROTOCOL)
        
        

def test_model(dataset,classif,datatest,classiftest,n_estimators,max_depth,critere,split,leaf):
    """Fonction qui entraine le modèle a partir des données d'entrainement et clacule les différents métriques

    Args:
        dataset (_array_): données d'entrainement
        classif (_array_): classe des données d'entrainement
        datatest (_array_): données test
        classiftest (_array_): classe des données test 
        n_estimators (_int_): Paramètres du randomForest
        max_depth (_int_): Paramètres du randomForest
        critere (_String_): Paramètres du randomForest
        split (_int_): Paramètres du randomForest
        leaf (_int_): Paramètres du randomForest

    Returns:
        _model, float_: Retourne le model créer et son score f1
    """
    print("Training")
    model = train_model(dataset,np.ravel(classif),n_estimators,max_depth,6,critere,split,leaf)
    print("Test")
    Classif_predict= model.predict(datatest)
    print(confusion_matrix(np.ravel(classiftest), Classif_predict))
    f1 = f1_score(np.ravel(classiftest), Classif_predict, average='weighted')
    plt.show()  
    print(f1)
    return model,f1


#Script 

dataname='config.txt'  #Nom (chemin) du fichier de configuration
programme_classification(dataname)   

    
    