Bonjour, Ce travail s'inscrit dans un projet réalisé au sein de l'unité de géomatique sur la segmentation sémantique, ceci est le readme du Programme du RandomForest, Ce programme permet d'entrainer et optimiser des modèles d'entrainement sur vos données par l'algorithme de RandomForest et de réalisé vos classifications de vos données avec un modèle créer 

  

Pour faire fonctionner ce programme, il vous faut dans un même dossier :  

- Le script Python 

- Le fichier de configuration  

- Vos données 

  

  

Bibliothèque utilisée : 

Numpy Version: 1.21.5 

Laspy Version: 2.2.0 

Scikit-learn Version: 0.21.3 

Matplotlib Version: 3.5.2 

  

Il vaut mieux privilégier un lancement par invite de commande sur anaconda : python script_opti_model.py 

En effet l'interface de mémoire renvoie trop facilement des erreurs de mémoire vive lorsque que les données sont trop conséquentes 

  

Les données doivent être sous la forme de fichier las. 

  

Le programme permet deux actions différentes : 

- Option 1 : Créer un modèle d'entrainement à partir de données d'entrainement  

- Option 2 : Classer des données à partir d'un modèle d'entrainement  

  

  

Pour faire tourner le programme il vous faut donc créer un fichier de config : "config.txt" et le mettre dans le dossier du code, Il existe un exemple de fichier de config pour chacune des deux options : 

  

Option 1 : Création d'un modèle d'entrainement à partir de plusieurs exemples : 

  

            Option :Training 

            Nb_data_entrainement :3 

            Nom_data_train  :tr_ZFSSensor1_split_1-class-Feature25.las 

                            :tr_ZFSSensor1_split_5-class-Feature25.las 

                            :tr_ZFSSensor1_split_6-class-Feature25.las 

            Nom_data_test   :tr_ZFSSensor1_split_4-class-Feature25.las 

            Nom_Feature :Verticality16 

                        :Verticality25 

                        :Linearity16 

                        :Planarity25 

                        :Planarity16 

                        :Numberneighbors25 

                        :Anisotropy25 

                        :Anisotropy10 

                        :Surfacevariation25 

                        :Sphericity25 

                        :Sphericity10 

            Nom_class   :classification 

            Calcul_plusieur_modèle :False 

            Nb_estimators  :[10] 

            Nb_min_samples :[2] 

            Nb_min_leaf    :[1] 

            Save_model     :True 

            Nom_model      :model_Test-1 

            Régularisation :True 

            Nombre_point_voisinage :500 

            Pourcentage :0.85 

  

  

Le fichier de Config, doit donc remplir les informations suivantes  

  

Option : Training pour indiquer au programme qu'on réalise un entrainement  

Nb_data_entrainement : Le nombre de données d'entrainement (nombre entier) 

Nom_data_train : Le nom ou chemin celon si elles sont dans le même dossier que le programme ou non des données d'entrainement 

Nom_data_test: Le nom de la données test (il y'en a forcément qu'une seule) 

Nom_Feature: Le nom des features qu'on veut utiliser dans notre algorithme de Random Forest 

Nom_class : Le nom de la colonne dans le fichier las qui comprend la classification des points (généralement labels ou classification) 

Calcul_plusieurs_modèle : L'algorithme va t'il calculé plusieurs modèles avec différents paramètres jusqu'à trouver le plus optimisé ? Si oui, True sinon mettre False 

Nb_estimators: Paramètre de l'algorithme de RandomForest , si une seule valeur ; [x], si plusieurs valeurs ; [x1, x2] 

Nb_min_samples: Paramètre de l'algorithme de RandomForest , si une seule valeur ; [x], si plusieurs valeurs ; [x1,x2] 

Nb_min_leaf : Paramètre de l'algorithme de RandomForest , si une seule valeur ; [x], si plusieurs valeurs ; [x1,x2] 

Save_model : Voulez-vous sauvegardez ou non le model, si oui ; True , si non ; False 

Nom_model : Le nom du fichier qui sauvegarde le model 

Régularisation : Voulez-vous testé une régularisation sur les données entrainement ; True si oui, False si non 

Nombre_point_voisinage : Nombre de point calculé au voisinage dans la régularisation 

Pourcentage : Pourcentage à partir duquel le point est considéré comme perdue  

  

  

  

Option 2 : Classer des données à partir d'un modèle d'entrainement : 

  

Le fichier de config est similaire à l'option 1 ;  

  

            Option : Classer 

            Name_model :model_Test-1 

            Nb_data :1 

            Nom_data :tr_ZFSSensor1_split_4-class-Feature25.las 

            Nom_Feature :Verticality16 

                        :Verticality25 

                        :Linearity16 

                        :Planarity25 

                        :Planarity16 

                        :Numberneighbors25 

                        :Anisotropy25 

                        :Anisotropy10 

                        :Surfacevariation25 

                        :Sphericity25 

                        :Sphericity10 

            Régularisation:True 

            Nombre_point_voisinage:500 

            Pourcentage :0.85 

  

Option : Classer pour indiquer qu'on va réaliser une classification de données a partir d'un model créer 

Name_model: Nom ou chemin du fichier model  

Nb_data: Nombre de data a classé 

Nom_data : Nom des données a classé 

Nom_Features: Nom des features utilisé pour la classification 

Régularisation : Réalise t-on une régularisation pour classer les données ? Si oui , True , si non, False 

Nombre_point_voisinage : Nombre de point calculé au voisinage dans la régularisation 

Pourcentage : Pourcentage a partir duquel le point est considéré comme perdue  

  

  

  

La régularisation est un algorithme implémenter en plus du RandomForest afin de corriger le bruit produit par le RandomForest, 

l'algortihme parcourt l'environnement de chaque point et s'il est entouré d'une autre classe a plus de x% (paramètre pourcentage) alors il se voit automatiquement attribuer cette classe. 

  

L'algortihme de RandomForest dépent de la bibliothèque Scikit-learn , pour plus d'information sur les différents paramètres :  https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html?highlight=randomforest#sklearn.ensemble.RandomForestClassifier 

  

Pour le choix des Features et du Nom_class , une limite de l'algortihme est qu'on ne peut pas utiliser le nom de feature ou colonne qui n'est pas implémenté de base dans l'algorithme. 

Si vous voulez utiliser des features qui ne sont pas implémentez il faut ajouter les deux lignes : 

  

        if 'nom_feature' in Lconfi: 

            dataset=np.vstack((dataset,las.nom_feature)) 

  

Dans la fonction Recuperation_feature 

  

  

Les features implémentés sont : green,red,blue,Verticality16,Verticality25,Verticality8,Linearity16,Linearity25,Linearity8,Planarity25,Planarity16,Planarity8,Numberneighbors25,Numberneighbors5,Numberneighbors8,Numberneighbors16, 

Anisotropy25,Anisotropy10,Anisotropy5,Anisotropy16,Surfacevariation25,Surfacevariation16,Surfacevariation8,Surfacevariation10,Surfacevariation5,Sphericity25,Sphericity10,Sphericity5,Sphericity16,Sphericity8 

  

Sphericity  est le nom pour la feature géométrique calculé avec un rayon de 5 cm 

  

On ne peut pas implémenter dans l'algorithme des noms de features contenant des espaces, du a l'utilisation de la bibliothèque laspy pour lire les fichiers las 

  

De même si le nom de la colonne qui comprend les données des classes n'est ni classification ou labels, il faudrat implémenter ce nouveau nom dans la fonction Recup_classe : 

  

  

def Recup_classe(Lconfi,dataname): 

    with laspy.open(dataname) as fh: 

        las = fh.read() 

        if Lconfi[-10]=='classification': 

            classif=np.array([las.classification]) 

        if Lconfi[-10]=='labels': 

            classif=np.array([las.labels]) 

        return classif 

  

Il faut rajouté suivant le model : 

        if Lconfi[-10]=='nom_colonne': 

            classif=np.array([las.nom_colonne]) 

             

  

  

Si vous voulez changez le fichier de config il suffit de changer le chemin ligne 469 du programme : 

dataname='config.txt' 